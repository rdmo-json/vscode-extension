# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2021-04-29

### Added

- More docs, QuickStart

### Changed

- Updated to newest schema version

### Fixed

- Prevent export commands from crashing when workspace is not a git repo
- Stop go to definitions from sometimes not working

## [0.2.1] - 2021-02-18

### Added

- Definition provider supporting the „Go to definition“ command”
  Finds domain attributes from references in questions and conditions

### Changed

- Use yaml instead of js-yaml package because of its AST features

## [0.2.0] - 2021-02-16

### Changed

- Update to current schema and XML serializer
- Writes XML files into configured workspace subfolder

### Added

- Tries to infer current version from git, if available
- Accept JSON and YAML files
- YAML validation contribution
- Menu entry in explorer context menu
- configuration settings for output directory and git version flag

### Removed

- Preview command

## [0.1.1] - 2021-01-28

### Fixed

- Updated xml serializer to newest bugfix version

## [0.1.0] - 2021-01-27

### Added

- Tries to infer the current catalog version from git
  using the [git-describe] package, if the catalog is contained in a git repo

[git-describe]: https://www.npmjs.com/package/git-describe

### Changed

- Updated to schema v0.7
- The bundle downloads schema file from release URL instead of gitlab pages,
  because the latter reflects the main branch and can frequently change.
- Removed the snippets from this bundle, because
  - They're difficult to maintain due to JSON escaping in snippet syntax
  - The schema contains its own snippets

## [0.0.5] - 2021-01-21

### Changed

- Updated to newest schema and xml writer
- There are only commands to export or preview the complete catalog data

## [0.0.4] - 2021-01-13

### Fixed

- Use patched version of xml serializer which outputs correct booleans

## [0.0.3] - 2021-01-11

### Fixed

- Syntax issues in snippet

## [0.0.2] - 2021-01-11

### Added

- Snippet for a new catalog
- JSON validation schema for *.rdmo.json files
- Preview commands

## [0.0.1] - 2021-01-08

- Initial release

[Unreleased]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.3.0...main
[0.3.0]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.2.1...0.3.0
[0.2.1]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.1.1...0.2.0
[0.1.1]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.0.5...0.1.0
[0.0.5]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.0.4...0.0.5
[0.0.4]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.0.3...0.0.4
[0.0.3]: https://gitlab.com/rdmo-json/vscode-extension/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/rdmo-json/vscode-extension/compare/null...0.0.2
[0.0.1]: https://gitlab.com/rdmo-json/vscode-extension/tags/v0.0.1

<!-- markdownlint-configure-file { "MD024": { "siblings_only": true }} -->
