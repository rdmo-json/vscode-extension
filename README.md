# RDMO JSON VSCode Extension

This extension can support your workflow
for writing RDMO catalogs in JSON or YAML format using my [JSON schema][schema].
It is still very basic, but it will gain more functionality.

## Features

- [x] Validation and autocompletion based on [schema]
- [x] XML export via [XML export package]
- [x] Infer catalog version from git
- [x] Navigation: Jump to definition from attribute reference
- [ ] Intellisense for attribute references
- [ ] Snippets for quicker data insertion

## Quickstart

### Install

This extension must be manually downloaded and installed,
because it is not yet published to market place.

- Visit the [releases page] and download the *.vsix file from the newest release.
- Open VSCode and go to Preferences > Extensions
- Search and install the [YAML extension] by Red Hat,
  unless you'll write exclusively in JSON.
- From the extensions context menu, select Install from VSIX
  and find the downloaded file
- Probably you get a success notification to reload VSCode. Hit reload and proceed.

### First catalog

You might be interested in familiarizing yourself with [editing JSON in VSCode].
Most of this also applies for YAML files.

- Create a new folder and open it in VSCode.
  You have created a so-called [Workspace].
- In that folder, create a new file with the `.rdmo.yml` extension, e.g., `rdmp-questions.rdmo.yml`.
  If you don't want to use this file extension,
  you will have to configure the schema manually.

### Intellisense

Your catalog is still empty. Press ⌃Space to see suggestions,
or if you already know what you want to write, begin typing.
You can navigate the suggestions with arrow keys and confirm with enter.

VSCode will report three missing properties in the problems view:
prefix, key, and title. Let's insert them via intellisense.
Either press ⌃Space and select prefix, or simply press p and confirm prefix.
You will end up at the correct position to enter your prefix value,
e.g., `http://rdmo.example.org/terms`.
Add a new line by pressing enter and try the same for key.

Title is a property where the schema allows for simple text string like key and prefix
or two nested properties named en and de with text values.
This kind of property is used in the schema for text which is displayed to RDMO users.

The result could look like this:

```yaml
prefix: http://example.rdmo.org/terms
key: example_catalog
title:
  de: Beispielkatalog
  en: Example catalog
````

### Domain

The domain is a list of entities, and each enty is either a string
or an objekt with one key where the value is again a list of entities:

```yaml
domain:
  - general:
      - id
      - title
      - description
  - data
  - metadata
  - archiving
````

Domain attributes are referenced in questions like URI paths,
e.g., `general/description`.
When your cursor is within an attribute reference,
you can press F12 to jump to the respective domain attribute.

### Questions

A catalog contains sections which contain questionsets which contain questions.
You can try to create some yourself or start with this example:

```yaml
sections:
  - key: general_information
    title:
      de: Allgemeine Projektinformationen
      en: General project information
    questionsets:
      - key: project
        title:
          de: Projekt
          en: Project
        questions:
          - key: project_title
            text:
              de: Bitte nennen Sie den Titel Ihres Projekts.
              en: Please provide the title of your project.
            attribute: general/title
            type: single-response
````

### Export to XML

It is possible to export the YAML file to XML from two places:

- editor title bar -> more actions: Compile currently open RDMO catalog to XML files
- explorer context menu: Compile RDMO catalog file to XML files

The XML files are written to the dist folder in the current workspace,
but you can configure the folder name via the outDir setting.
See [settings] for an overview about how to configure VSCode.

### Versioning

The catalog has an optional version property. If it is set,
its value will be appended to any element keys in generated XML files.
This prevents elements from being overwritten by newer elements.
See [Semantic Versioning] for a formal specification of version strings.
If you don't want to use versioning, skip the version property.

If the catalog workspace is also a git repository,
the extension will try to infer the current version from git tags
when you export to XML.
You can also disable the respective setting (gitVersion)
and maintain the version manually,
but you can avoid redundancy if you use git for version control
and tag the version commits according to a specification like SemVer.

[schema]: https://gitlab.com/rdmo-json/schema
[XML export package]: https://gitlab.com/rdmo-json/xml
[releases page]: https://gitlab.com/rdmo-json/vscode-extension/-/releases
[YAML extension]: https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml
[editing JSON in VSCode]: https://code.visualstudio.com/Docs/languages/json
[Workspace]: https://code.visualstudio.com/docs/editor/workspaces
[settings]: https://code.visualstudio.com/docs/getstarted/settings
[Semantic Versioning]: https://semver.org
