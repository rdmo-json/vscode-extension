declare module 'git-describe' {
  export type GitDescribeCallback = (arg0: Error, arg1: GitInfo) => void
  export interface GitDescribeOptions {
    dirtyMark?: string
    dirtySemver?: boolean
    longSemver?: boolean
    requireAnnotated?: boolean
    match?: string|string[]
    customArguments?: string[]
  }
  export interface GitInfo {
    dirty: boolean
    hash: string
    distance: number
    tag: string
    semver: SemVer|null
    semverString: string|null
    suffix: string
    raw: string
  }
  export function gitDescribe (project?: string, options?: GitDescribeOptions): Promise<GitInfo>
  export function gitDescribe (project?: string, options?: GitDescribeOptions, cb?: GitDescribeCallback): void
  export function gitDescribeSync (project?: string, options?: GitDescribeOptions): GitInfo
  import { SemVer } from 'semver'
}
