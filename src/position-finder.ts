import { Node, Scalar, Pair, YAMLMap, YAMLSeq } from 'yaml/types'
import * as YAML from 'yaml'

interface Result {
  type: string
  value: string
}

export class PositionFinder {
  result: Result|null = null
  constructor (public position: number) {}
  Seq (_: any, node: YAMLSeq): symbol|undefined {
    if (node.range === undefined || node.range === null) { return }
    if (this.behind(node.range)) { return YAML.visit.BREAK }
    if (!this.between(node.range)) { return YAML.visit.SKIP }
  }

  Map (_: any, node: YAMLMap): symbol|undefined {
    if (node.range === undefined || node.range === null) { return }
    if (this.behind(node.range)) { return YAML.visit.BREAK }
    if (!this.between(node.range)) { return YAML.visit.SKIP }
  }

  Scalar (key: any, node: Scalar, path: Node[]): symbol|undefined {
    if (node.range === undefined || node.range === null) { return }
    if (this.behind(node.range)) { return YAML.visit.BREAK }
    if (!this.between(node.range)) { return }
    this.result = this.questionAttribute(key, node, path)
    if (this.result === null) {
      this.result = this.conditionAttribute(key, node, path)
    }
    return YAML.visit.BREAK
  }

  questionAttribute (key: any, node: Scalar, path: Node[]): Result|null {
    if (key !== 'key' && key !== 'value') { return null }
    const pair = path[path.length - 1] as Pair
    if (pair.key.value !== 'attribute') { return null }
    return { type: 'attributeReference', value: pair.value.value }
  }

  conditionAttribute (key: any, node: Scalar, path: Node[]): Result|null {
    if (path.length < 3) { return null }
    if (typeof key !== 'number' || key !== 0) { return null }
    const collection = path[path.length - 1]
    if (collection.type !== 'SEQ' && collection.type !== 'FLOW_SEQ') { return null }
    const grandCollection = path[path.length - 2]
    if (grandCollection.type !== 'SEQ' && grandCollection.type !== 'FLOW_SEQ') { return null }
    const pair = path[path.length - 3]
    if (pair.type !== 'PAIR' || (pair as Pair).key.value !== 'conditions') { return null }
    return { type: 'attributeReference', value: node.value }
  }

  between ([start, end]: [number, number]): boolean {
    return this.position >= start && this.position <= end
  }

  behind ([start, end]: [number, number]): boolean {
    return this.position < start
  }
}
