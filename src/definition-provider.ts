import type { DefinitionProvider, Definition, ProviderResult, TextDocument, Position } from 'vscode'
import type { Collection, Scalar } from 'yaml/types'
import { Location, Range } from 'vscode'
import * as YAML from 'yaml'
import { PositionFinder } from './position-finder'

export class RDMODefinitionProvider implements DefinitionProvider {
  provideDefinition (document: TextDocument, position: Position): ProviderResult<Definition> {
    const doc = YAML.parseDocument(document.getText())
    const visitor = new PositionFinder(document.offsetAt(position))
    YAML.visit(doc, visitor)
    if (visitor.result === null) { return }
    const path = visitor.result.value.split('/')
    const domain = doc.get('domain')
    const attribute = this.findAttribute(domain, path)
    if (attribute === undefined) { return }
    if (attribute.range === undefined || attribute.range === null) { return }
    const [start, end] = attribute.range.map(n => document.positionAt(n))
    return new Location(document.uri, new Range(start, end))
  }

  findAttribute (domain: Collection, path: string[]): Scalar|undefined {
    if (path.length === 0) { return }
    const segment = path[0]
    if (path.length === 1) {
      return domain.items.find(n => n.value === segment)
    }
    const item = domain.items.find(node => {
      if (node.type === 'PLAIN') { return false }
      return node.has(segment)
    })
    if (item === undefined) { return }
    const child = item.get(segment)
    return this.findAttribute(child, path.slice(1))
  }
}
