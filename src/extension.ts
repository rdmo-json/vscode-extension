import type { ExtensionContext } from 'vscode'
import type { Catalog } from '@rdmo-author/schema'
import { Uri, commands, languages, window, workspace } from 'vscode'
import { write } from '@rdmo-author/xml'
import { gitDescribe } from 'git-describe'
import * as YAML from 'yaml'
import { RDMODefinitionProvider } from './definition-provider'

export function activate ({ subscriptions }: ExtensionContext): void {
  const gitDescribeOptions = { match: ['[0-9]*', 'v[0-9]*'] }
  const rdmoChannel = window.createOutputChannel('RDMO')
  const compileHandler = async (resource: Uri): Promise<void> => {
    rdmoChannel.appendLine('Trying to parse content from active text editor')
    if (window.activeTextEditor === undefined) { return }
    const config = workspace.getConfiguration('rdmo')
    if (workspace.workspaceFolders === undefined) {
      rdmoChannel.appendLine('There is no workspace to write the XML files to')
      return
    }
    const root = workspace.workspaceFolders[0]
    const data = parseText(window.activeTextEditor.document.getText(), window.activeTextEditor.document.languageId)
    if (data === null) {
      rdmoChannel.appendLine('Failed to parse content from active text editor')
      return
    }
    rdmoChannel.appendLine('Successfully parsed content')
    if (config.get('gitVersion') === true) {
      try {
        const gitInfo = await gitDescribe(root.uri.fsPath, gitDescribeOptions)
        if (gitInfo.semverString !== null) {
          data.version = gitInfo.semverString
          rdmoChannel.appendLine(`Current version from git: ${gitInfo.semverString}`)
        }
      } catch (e) {
        rdmoChannel.appendLine('Failed to parse git version from workspace')
      }
    }
    try {
      const xml = write(data)
      rdmoChannel.appendLine('Successfully created xml')
      const outDir = Uri.joinPath(root.uri, config.get('outDir') ?? 'dist')
      await workspace.fs.createDirectory(outDir)
      rdmoChannel.appendLine(`Created output directory: ${outDir.fsPath}`)
      for (const [name, value] of Object.entries(xml)) {
        const filename = `${name}.xml`
        await workspace.fs.writeFile(Uri.joinPath(outDir, filename), Buffer.from(value, 'utf8'))
        rdmoChannel.appendLine(`${filename} has been written to ${outDir.fsPath}`)
      }
    } catch (e) {
      rdmoChannel.appendLine(`Failed to create XML from catalog: ${JSON.stringify(e.message)}`)
    }
  }
  subscriptions.push(commands.registerCommand('rdmo.compile', compileHandler))

  const compileFileHandler = async (resource: Uri): Promise<void> => {
    rdmoChannel.appendLine(`Trying to compile ${resource.fsPath}`)
    const config = workspace.getConfiguration('rdmo')
    if (workspace.workspaceFolders === undefined) {
      rdmoChannel.appendLine('There is no workspace to write the XML files to')
      return
    }
    const root = workspace.workspaceFolders[0]
    const buf = await workspace.fs.readFile(resource)
    const data = parseText(buf.toString(), 'yaml')
    if (data === null) {
      rdmoChannel.appendLine('Failed to parse content from active text editor')
      return
    }
    rdmoChannel.appendLine('Successfully parsed content')
    if (config.get('gitVersion') === true) {
      try {
        const gitInfo = await gitDescribe(root.uri.fsPath, gitDescribeOptions)
        if (gitInfo.semverString !== null) {
          data.version = gitInfo.semverString
          rdmoChannel.appendLine(`Current version from git: ${gitInfo.semverString}`)
        }
      } catch (e) {
        rdmoChannel.appendLine('Failed to parse git version from workspace')
      }
    }
    try {
      const xml = write(data)
      rdmoChannel.appendLine('Successfully created xml')
      const outDir = Uri.joinPath(root.uri, config.get('outDir') ?? 'dist')
      await workspace.fs.createDirectory(outDir)
      rdmoChannel.appendLine(`Created output directory: ${outDir.fsPath}`)
      for (const [name, value] of Object.entries(xml)) {
        const filename = `${name}.xml`
        await workspace.fs.writeFile(Uri.joinPath(outDir, filename), Buffer.from(value, 'utf8'))
        rdmoChannel.appendLine(`${filename} has been written to ${outDir.fsPath}`)
      }
    } catch (e) {
      rdmoChannel.appendLine(`Failed to create XML from catalog: ${JSON.stringify(e.message)}`)
    }
  }
  subscriptions.push(commands.registerCommand('rdmo.compileFile', compileFileHandler))
  subscriptions.push(languages.registerDefinitionProvider(
    [{ language: 'json' }, { language: 'yaml' }],
    new RDMODefinitionProvider()
  ))

  function parseText (text: string, language: string): Catalog|null {
    switch (language) {
      case 'yaml': {
        const parsed = YAML.parse(text)
        if (parsed === null || typeof parsed !== 'object') { return null }
        return parsed as Catalog
      }
      case 'json': {
        try {
          return JSON.parse(text)
        } catch {
          return null
        }
      }
      default:
        return null
    }
  }
}
